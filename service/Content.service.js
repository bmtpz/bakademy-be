const Content = require("../model/Content.model");
/**
 * Exporting functions in an object
 * @service
 */
let contentService = {
    createContent
}

async function createContent (content) {
    return await Content.create(content)
}


module.exports = contentService