/**
 * Course not found exception
 * @function
 * @Exception
 */
module.exports = function CourseNotFoundException() {
    this.status = 404
    this.message = "Course not found" 
}