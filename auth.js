require("dotenv").config();
const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");
const app = express();
const db = require("./config/db");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const ErrorHandler = require("./error/ErrorHandler");
const userService = require("./service/User.service");
const Token = require("./model/Token.model");
const { regValidate } = require("./middleware/validation/user/reg");
const { loginValidate } = require("./middleware/validation/user/login");
const JWTVerify = require("./middleware/JWTVerify");
const ValidationException = require("./error/ValidationException");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.options("*", cors());

db.authenticate()
  .then(() => console.log("Connection has been established successfully."))
  .catch(() => console.error("Unable to connect to the database:", error));

app.post("/api/v1/token", async (req, res) => {
  const refreshToken = req.body.token;
  if (refreshToken == null) return res.sendStatus(401);
  const dbToken = await Token.findOne({
    attributes: ["token"],
    where: { token: refreshToken },
  });
  if (!dbToken) return res.sendStatus(403);
  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    const accessToken = generateAccessToken(user.id);
    res.json({ accessToken: accessToken });
  });
});

app.post("/api/v1/auth/register", regValidate, (req, res) => {
  const user = req.body;
  userService.addUser(user);
  res.json("user registered successfully");
});

app.post("/api/v1/auth/login", loginValidate, async (req, res, next) => {
  const user = await userService.getUserByEmail(req.body.email);
  if (user === null) {
    return res.status(400).json("Can not find user");
  }
  const tokenUser = {
    id: user.id,
    username: user.username,
    email: user.email,
  };

  try {
    if (await bcrypt.compare(req.body.password, user.password)) {
      const accessToken = generateAccessToken(user.id);
      const refreshToken = await jwt.sign(
        tokenUser,
        process.env.REFRESH_TOKEN_SECRET
      );
      Token.create({
        id: user.id,
        token: refreshToken,
      });

      res.status(200).json({
        user: tokenUser,
        message: "login successfull",
        accessToken: accessToken,
        refreshToken: refreshToken,
      });
    } else {
      throw new ValidationException([
        { msg: "Invalid password", param: "password" },
      ]);
    }
  } catch (err) {
    next(err);
  }
});

app.delete(
  "/api/v1/auth/logout",
  JWTVerify.authenticateToken,
  async (req, res) => {
    const token = await Token.findByPk(req.userId);
    if (!token) {
      res.status(403).json("Forbidden");
    }
    token.destroy();
    res.status(204).json("logged out succesfully");
  }
);

function generateAccessToken(user) {
  return jwt.sign({ id: user }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "10m",
  });
}
app.use(ErrorHandler);
db.sync().then(() => {
  app.listen(4000, () => console.log("server started"));
});
