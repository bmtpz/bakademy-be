const jwt = require('jsonwebtoken')
require('dotenv').config()
let JWTVerify = { authenticateToken }
/**
 * Middleware for verfying jwt
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 * @returns 
 */
function authenticateToken (req, res, next) {
    const autHeader = req.headers['authorization']
    const token = autHeader && autHeader.split(' ')[1]
    if (token === null) return res.status(401).json('Unauthorized')

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) return res.status(403).json('Forbidden')
        req.userId = user.id
        next()
    })
}

module.exports = JWTVerify