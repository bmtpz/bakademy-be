const { body, validationResult } = require('express-validator');
const ValidationException = require('../../../error/ValidationException');
const userService = require('../../../service/User.service');
/**
 * an Array of rules to validate body
 */
exports.loginValidate = [
    body('email').isEmail().withMessage('Must be a valid e-mail address')
        .custom(async (email) => {
            const user = await userService.getUserByEmail(email)
            if (!user) {
                throw new Error('user does not exist')
            }
        }),
    body('password')
        .notEmpty().withMessage('Password cannot be null')
        .bail()
        .isStrongPassword().withMessage('password must contain at least one Upper case, one lower case, one number, one special character and must be at least 8 characters long'),
    /**
* Middleware function for throw validation errors or move on with next layer
* @param {Express.Request} req 
* @param {Express.Response} res 
* @param {Express.Response} next 
*/
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty())
            return next(new ValidationException(errors.array()));
        next();
    },
];