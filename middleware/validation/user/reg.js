const { body, validationResult } = require('express-validator');
const ValidationException = require('../../../error/ValidationException');
const userService = require('../../../service/User.service');
/**
 * an Array of rules to validate body
 */
exports.regValidate = [
    body('username').notEmpty()
        .withMessage('Username cannot be null')
        .bail()
        .isLength({ min: 4, max: 10 }).withMessage('Username must have min 4 and max 10 characters'),
    body('email').isEmail().withMessage('Must be a valid e-mail address')
        .custom(async (email) => {
            const user = await userService.getUserByEmail(email)
            if (user) {
                throw new Error('This email is already in use')
            }
        }),
    body('firstName')
        .notEmpty().withMessage('First name cannot be null')
        .bail()
        .isString().withMessage('Please only enter string')
        .bail()
        .isLength({ max: 50 }).withMessage('First name cannot be more than 50 characters'),
    body('lastName')
        .notEmpty().withMessage('Last name cannot be null')
        .bail()
        .isString().withMessage('Please only enter string')
        .bail()
        .isLength({ max: 50 }).withMessage('First name cannot be more than 50 characters'),
    body('password')
        .notEmpty().withMessage('Password cannot be null')
        .bail()
        .isStrongPassword().withMessage('password must contain at least one Upper case, one lower case, one number, one special character and must be at least 8 characters long'),
    /**
    * Middleware function for throw validation errors or move on with next layer
    * @param {Express.Request} req 
    * @param {Express.Response} res 
    * @param {Express.Response} next 
    */
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty())
            return next(new ValidationException(errors.array()));
        next();
    },
];