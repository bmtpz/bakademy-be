const InvalidIdException = require('../error/InvalidIdException')
/**
 * Middleware for checking id param is a number
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
module.exports = (req, res, next) => {
    const id = Number.parseInt(req.params.id);
    if (Number.isNaN(id)) {
        throw new InvalidIdException();
    }
    next();
}