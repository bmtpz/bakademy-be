const express = require("express");
const categoryController = require("../controller/Category.controller");
const IdControl = require("../middleware/IdControl");
const JWTVerify = require("../middleware/JWTVerify");
const router = express.Router();

router.get("/", categoryController.getCategories)
router.get('/:id', JWTVerify.authenticateToken, IdControl, categoryController.getCategoryByID)

module.exports = router