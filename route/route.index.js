const express = require('express')
const router = express.Router()
const userRoutes = require('./User.route')
const courseRoutes = require('./Course.route')
const categoryRoutes = require('./Category.route')
const JWTVerify = require('../middleware/JWTVerify')

router.use('/categories', categoryRoutes)
router.use('/courses', courseRoutes)
router.use('/users', JWTVerify.authenticateToken, userRoutes)
module.exports = router