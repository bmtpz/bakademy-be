const bodyParser = require('body-parser')
const express = require('express')
const cors = require('cors')
const app = express()
const path = require('path')
const db = require('./config/db')
const ErrorHandler = require('./error/ErrorHandler')
//
const Course = require('./model/Course.model')
const categoryData = require('./config/data/categories.json')
const Category = require('./model/Category.model')
const userService = require('./service/User.service')
const categoryService = require('./service/Category.service')
const Role = require('./model/Role.model')
//
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.use(cors());
app.options("*", cors());
app.use('/api/v1', require('./route/route.index'))
app.use(ErrorHandler)
db.authenticate()
.then(()=>console.log('Connection has been established successfully.')
).catch(()=>console.error('Unable to connect to the database:', error)
)


db.sync({alter:true}).then( async () => {
    // const users = await userService.getUsers()
    // const userRole= await Role.findByPk(3)
    // users.forEach(async user => {
    //     await user.addRole(userRole)
    // })
    // const ins =await userService.getUserByID(21)
    // const insRole= await Role.findByPk(2)
    // const adminrole = await Role.findByPk(1)
    // await ins.addRole(insRole)
    // await ins.addRole(adminrole)
    // Role.create({roleName:'Admin'})
    // Role.create({roleName:'Instructor'})
    // Role.create({roleName:'User'})

    // categoryData.forEach(async (item) => {
    //     await Category.create(item)
    // })
    // for (let i=1; i<=20; i++) {
    //     let user = {
    //         username: `user${i}`,
    //         firstName: `user${i}_first_name`,
    //         lastName: `user${i}_last_name`,
    //         email: `user${i}@mail.com`,
    //         password: `user${i}Pass`
    //     }
    //     await userService.addUser(user)
        
    // }
    // const ins =await userService.getUserByID(1)
    // for(let i=1; i<=20; i++){
    //     let course = {
    //         courseName: `course${i}`,
    //         description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
    //         coverImg: "https://res.cloudinary.com/bakademy/image/upload/v1642503680/bakademy/javascript-ders_uutl1q.png"
    //     }
        
    //     let cat_id = Math.floor(Math.random() * (9 - 3) + 3)
    //     let cr = await Course.create(course)
    //     let cat = await categoryService.getCategoryByID(cat_id)
    //     await cr.addCategory(cat)
    //     await cr.setInstructor(ins)
    // }
    app.listen(3000, () => console.log('server started'))
})