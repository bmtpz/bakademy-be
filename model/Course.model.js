const { get } = require("dottie");
const {Sequelize, DataTypes} = require("sequelize");
const db = require("../config/db");
const Category = require("./Category.model");
/**
 * Represents Course model
 */
const Course = db.define('Courses', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    courseName: {
        type: DataTypes.STRING(200),
        allowNull: false
    },
    description: {
        type: DataTypes.STRING(1000),
        allowNull: false
    },
    price: {
        type: DataTypes.FLOAT,
        defaultValue: 0,
        allowNull: false
    },
    coverImg:{
      type: DataTypes.STRING
    },
    score: {
        type: DataTypes.FLOAT,
        defaultValue: 0,
        allowNull: false
    },
    scoreCounter: {
      type: DataTypes.INTEGER,
      allowNull:false,
      defaultValue: 0
    },
    rate: {
      type: DataTypes.VIRTUAL,
      
      get () {
        const rate = this.score/ this.scoreCounter
        if (!rate) {
          return 0
        }
        return rate
      },
      defaultValue: 0
    }
  });

  //ASSOCIATIONS

  /** Defines Courses to Categories relation */
  Course.belongsToMany(Category, {
    through: "Courses_Categories",
    onDelete: "cascade",
  });
  
  /** Defines Categories to Courses relation */
  Category.belongsToMany(Course, {
    through: "Courses_Categories",
    onDelete: "cascade",
  });

module.exports = Course