const {Sequelize, DataTypes} = require("sequelize");
const db = require("../config/db");
const Course = require("./Course.model");
const Users_Courses = require("./Users_Courses.model");
/**
 * Represents User model
 */
const User = db.define('Users', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    firstName: {
        type: DataTypes.STRING(50)
    },
    lastName: {
        type: DataTypes.STRING(50)
    },
    email: {
        type: DataTypes.STRING(30)
    },
    fullName: {
        type: DataTypes.VIRTUAL,
        get(){
            return this.firstName + ' ' + this.lastName
        }
    }

  });

  //ASSOCIATIONS

  /** Defines relation Users to Courses */
  User.belongsToMany(Course, {
    through: Users_Courses,
    onDelete: "cascade",
  });
  
  /** Defines relation to Courses To Users */
  Course.belongsToMany(User, {
    through: Users_Courses,
    onDelete: "cascade",
  });

  Course.belongsTo(User,{as:'instructor'})
  User.hasMany(Course)
  

module.exports = User